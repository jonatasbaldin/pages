<!DOCTYPE html>

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5">

        
        <link rel="stylesheet" href="/style.css">
        
        
        <title>API Design: Think First, Code Later | jardim do jojo</title>
        
    </head>

    <body>

        <header>
            <h1>
                <a href="/">jardim do jojo</a>
            </h1>
        </header>

        <nav>
            <a href="/">/início</a>
            <a href="/tech">/tech</a>
            <a href="/outros">/outros</a>

            

        </nav>

        <main>
            

<div class="single">
    <h1 class="title">API Design: Think First, Code Later</h1>
    <i><strong>// </strong>Publicado em: 22 de junho de 2017</i>

    

    <p><em>Originally posted at <a href="https://cheesecakelabs.com/blog/api-design-think-first-code-later/">Cheesecake Labs</a>.</em></p>
<p>As a software developer, I know how hard it is to contain the urge to start coding as soon as we can. After the first sprint planning, our fingers – uncontrolled, hungry creatures – want to start smashing the keyboard, translating our ideas into code fastly and furiously.</p>
<p>Despite how great we feel while developing, it&rsquo;s always a good idea to take a step back, especially when building something that could be used by many different users – like an API is. A. In this post, I’ll show you why and how to design a properly-thought API.</p>
<p>API is a generic term to define an Application Program Interface, in other words, how a user (human or machine) interacts with a program. In the web development world, an API is generally a website with a collection of endpoints that respond to client requests with structured text data.</p>
<p>Another concept that&rsquo;s widely used and discussed by web developers is the one of a <a href="https://en.wikipedia.org/wiki/Representational_state_transfer">RESTFul Web API</a>. It was defined by <a href="https://en.wikipedia.org/wiki/Roy_Fielding">Roy Fielding</a> as an architecture style that provides a well-established communication protocol between client and server. It outlines some constraints: stateless communication, respect of the underlying technology (generally HTTP) and the use of <a href="http://en.wikipedia.org/wiki/HATEOAS">hypermedia as the engine of application state</a>. In other words, it proposes some patterns for building a web API. For simplicity&rsquo;s sake, I&rsquo;ll be referring to Web APIs simply as APIs throughout this post.</p>
<h1 id="one-json-is-worth-a-thousand-words">One JSON is worth a thousand words</h1>
<p>Let’s see an example of an API response:</p>
<div class="highlight"><pre tabindex="0" style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4;"><code class="language-json" data-lang="json"><span style="display:flex;"><span>{
</span></span><span style="display:flex;"><span>  <span style="color:#f92672">&#34;data&#34;</span>: [
</span></span><span style="display:flex;"><span>    {
</span></span><span style="display:flex;"><span>      <span style="color:#f92672">&#34;story&#34;</span>: <span style="color:#e6db74">&#34;Jonatas Baldin was writing a blog post.&#34;</span>,
</span></span><span style="display:flex;"><span>       <span style="color:#f92672">&#34;created_time&#34;</span>: <span style="color:#e6db74">&#34;2017-15-01T00:02:57+0000&#34;</span>,
</span></span><span style="display:flex;"><span>       <span style="color:#f92672">&#34;id&#34;</span>: <span style="color:#e6db74">&#34;624510964324764_1046909755418214&#34;</span>
</span></span><span style="display:flex;"><span>    },
</span></span><span style="display:flex;"><span>  ]
</span></span><span style="display:flex;"><span>}
</span></span></code></pre></div><p>This snippet of data, called <a href="https://www.w3schools.com/js/js_json_intro.asp">JSON,</a> is an example of how a smartphone sees a Facebook status message, gathered from the <a href="https://developers.facebook.com/docs/graph-api">Facebook API</a>. Pretty neat, right?</p>
<p>Now imagine that you are an engineer at Facebook, responsible for this API. You wake up one day and decide to change the id field name to message_id. Well, this small change could break Facebook. For real. All devices that depend on the previously-defined structure would now stop being able to collect and display the content to the user. Definitely not a great day at work.</p>
<h1 id="the-good-the-bad-and-the-ugly">The good, the bad and the ugly</h1>
<p>A bad API design – or the lack of it – will, sooner or later, cause all kinds of trouble:</p>
<ul>
<li><strong>Absence of consistency</strong>: once an API grows, the endpoints tend to be created just to fulfill immediate needs.</li>
<li><strong>Difficulty to scale</strong>: there’s no reference when troubleshooting an endpoint.</li>
<li><strong>Hard to learn</strong>: a steep learning curve to consume data from and develop the API.</li>
<li><strong>Performance issues</strong>: throwing unplanned API code usually creates performance bottlenecks on the long run.</li>
<li><strong>APIs tend to be forever</strong>: it’s always better to get it right at the first chance.</li>
</ul>
<p>An API is a contract between your application and its users. It can’t be changed abruptly without causing chaos for those who already signed it and it shouldn&rsquo;t be built without forethought. This is where <a href="https://en.wikipedia.org/wiki/Design">design</a> enters: the creation of a plan or convention for the construction of an object, system or measurable human interaction.</p>
<h1 id="first-things-first-lets-talk-http">First things first: Let&rsquo;s talk HTTP</h1>
<p>Before diving into API code, let’s get a glimpse of the Hypertext Transfer Protocol. HTTP, as the name suggests, is used to transfer data on the web. It has a set of rules on how clients and servers should exchange information. Its main components are:</p>
<ul>
<li><strong>URL</strong>: where your resources are on the web, the address of your endpoint. An example is using <a href="http://example.org/users">http://example.org/users</a> to list your users.</li>
<li><strong>Request methods</strong>: the action a client wants to perform on a specific endpoint. GET is used to retrieve a resource, POST, to create one, PUT and PATCH to update an existing one, and DELETE… well, deletes stuff.</li>
<li><strong>Headers</strong>: contain information about the client or the server. For instance: content type (format), method, authentication token and others.</li>
<li><strong>Body</strong>: the data sent to the server or received by the client. JSON is the de facto standard.</li>
<li><strong>Status code</strong>: a three-digit number which tells the request status. To summarize, 2xx statuses means success, 4xx means client errors and 5xx means service errors. You can get a full list <a href="https://http.cat/">here</a> or <a href="https://httpstatusdogs.com/">here</a>.</li>
</ul>
<p>Let&rsquo;s see an HTTP request/response example:</p>
<div class="highlight"><pre tabindex="0" style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4;"><code class="language-json" data-lang="json"><span style="display:flex;"><span><span style="color:#960050;background-color:#1e0010">Client</span> <span style="color:#960050;background-color:#1e0010">request:</span>
</span></span><span style="display:flex;"><span><span style="color:#960050;background-color:#1e0010">GET</span> <span style="color:#960050;background-color:#1e0010">/users/</span><span style="color:#ae81ff">1</span> <span style="color:#960050;background-color:#1e0010">HTTP/</span><span style="color:#ae81ff">1.1</span>
</span></span><span style="display:flex;"><span><span style="color:#960050;background-color:#1e0010">Host:</span> <span style="color:#960050;background-color:#1e0010">www.example.org/users</span>
</span></span><span style="display:flex;"><span>
</span></span><span style="display:flex;"><span><span style="color:#960050;background-color:#1e0010">Server</span> <span style="color:#960050;background-color:#1e0010">response:</span>
</span></span><span style="display:flex;"><span><span style="color:#960050;background-color:#1e0010">HTTP/</span><span style="color:#ae81ff">1.1</span> <span style="color:#ae81ff">200</span> <span style="color:#960050;background-color:#1e0010">OK</span>
</span></span><span style="display:flex;"><span><span style="color:#960050;background-color:#1e0010">Content-Type:</span> <span style="color:#960050;background-color:#1e0010">application/json</span>
</span></span><span style="display:flex;"><span><span style="color:#960050;background-color:#1e0010">Date:</span> <span style="color:#960050;background-color:#1e0010">Sun,</span> <span style="color:#ae81ff">19</span> <span style="color:#960050;background-color:#1e0010">Feb</span> <span style="color:#ae81ff">2017</span> <span style="color:#ae81ff">00</span><span style="color:#960050;background-color:#1e0010">:</span><span style="color:#ae81ff">43</span><span style="color:#960050;background-color:#1e0010">:</span><span style="color:#ae81ff">51</span> <span style="color:#960050;background-color:#1e0010">GMT</span>
</span></span><span style="display:flex;"><span>
</span></span><span style="display:flex;"><span>{
</span></span><span style="display:flex;"><span>  <span style="color:#f92672">&#34;id&#34;</span>: <span style="color:#ae81ff">1</span>,
</span></span><span style="display:flex;"><span>  <span style="color:#f92672">&#34;first_name&#34;</span>: <span style="color:#e6db74">&#34;Jonatas&#34;</span>,
</span></span><span style="display:flex;"><span>  <span style="color:#f92672">&#34;fast_name&#34;</span>: <span style="color:#e6db74">&#34;Baldin&#34;</span>,
</span></span><span style="display:flex;"><span>  <span style="color:#f92672">&#34;role&#34;</span>: <span style="color:#e6db74">&#34;Caker&#34;</span>
</span></span><span style="display:flex;"><span>}
</span></span></code></pre></div><h1 id="good-api-design-demands-good-specifications">Good API design demands good specifications</h1>
<p>There are some specifications to help you out when designing your API. The most used ones are <strong>Swagger</strong> with pure JSON, <strong>RAML</strong> with YAML notation and <strong>API Blueprint</strong> backed by the markdown syntax. I felt in love with the latter: even though it is the new cool kid in the neighborhood, it’s already mature enough and delightful to work with. That’s the one I’ll be covering here.</p>
<p>From the official website:</p>
<blockquote>
<p>API Blueprint is simple and accessible to everybody involved in the API lifecycle. Its syntax is concise yet expressive. With API Blueprint you can quickly design and prototype APIs to be created or document and test already deployed mission-critical APIs.</p>
</blockquote>
<p>It’s an open source, focused on collaboration, easy to learn and well-documented specification created by Apiary with design-first in its core, surrounded by awesome tools: from mock server generators to full-feature life-cycle solutions.</p>
<p>In addition to Blueprint, there’s <strong>MSON</strong> (Markdown Syntax Object Notation), which defines data structures in a human-readable way. Instead of writing manually the endpoints body data, you represent them in reusable objects. Pretty nifty, huh?</p>
<p>Here’s what you need to know to get started:</p>
<ul>
<li><strong>API Name, Description and Metadata</strong>: describe a little bit about the API and the Blueprint version.</li>
<li><strong>Resource Groups</strong>: group of related resources, like Users.</li>
<li><strong>Resource</strong>: Defines a unique resource, its endpoint and action.</li>
<li><strong>Parameters</strong>: Used in an endpoint to specify a dynamic parameter, like an ID or query search.</li>
<li><strong>Response</strong>: The content-type, HTTP status code and body data.</li>
</ul>
<p>Besides that, there’s <a href="https://apiary.io/">Apiary</a>. It’s a collaborative platform to create, render, test and serve your API. They have a free plan for public projects and you can sign up directly from your GitHub account to create your own designs.</p>
<p>Here’s a Blueprint code sample:</p>
<div class="highlight"><pre tabindex="0" style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4;"><code class="language-bash" data-lang="bash"><span style="display:flex;"><span>FORMAT: 1A
</span></span><span style="display:flex;"><span>HOST: http://cakes.ckl.io/
</span></span><span style="display:flex;"><span>
</span></span><span style="display:flex;"><span><span style="color:#75715e"># Cakes API</span>
</span></span><span style="display:flex;"><span>Cakes is an API used to store and consume information about the most loved Cakes here at Cheesecake Labs.
</span></span><span style="display:flex;"><span>
</span></span><span style="display:flex;"><span><span style="color:#75715e"># Group Cakes</span>
</span></span><span style="display:flex;"><span>
</span></span><span style="display:flex;"><span><span style="color:#75715e">## Cakes [/cakes/]</span>
</span></span><span style="display:flex;"><span>
</span></span><span style="display:flex;"><span><span style="color:#75715e">### List all Cakes [GET]</span>
</span></span><span style="display:flex;"><span>
</span></span><span style="display:flex;"><span>+ Response <span style="color:#ae81ff">200</span> <span style="color:#f92672">(</span>application/json<span style="color:#f92672">)</span>
</span></span><span style="display:flex;"><span>
</span></span><span style="display:flex;"><span>+ Attributes <span style="color:#f92672">(</span>array<span style="color:#f92672">[</span>Cake<span style="color:#f92672">])</span>
</span></span><span style="display:flex;"><span>
</span></span><span style="display:flex;"><span><span style="color:#75715e"># Data Structures</span>
</span></span><span style="display:flex;"><span>
</span></span><span style="display:flex;"><span><span style="color:#75715e">## Cake (object)</span>
</span></span><span style="display:flex;"><span>+ id: <span style="color:#e6db74">`</span>1<span style="color:#e6db74">`</span> <span style="color:#f92672">(</span>string, required<span style="color:#f92672">)</span> - The unique ID of a Cake.
</span></span><span style="display:flex;"><span>+ name: <span style="color:#e6db74">`</span>Cheesecake<span style="color:#e6db74">`</span> <span style="color:#f92672">(</span>string, required<span style="color:#f92672">)</span> - The name of a Cake.
</span></span><span style="display:flex;"><span>+ rating: <span style="color:#e6db74">`</span>5/5<span style="color:#e6db74">`</span> <span style="color:#f92672">(</span>string, optional<span style="color:#f92672">)</span> - The rating of a Cake.
</span></span></code></pre></div><p>If you access the <a href="http://docs.cheesecakelabs.apiary.io/">project</a> above, you’ll see a prettily-rendered page. Now the awesome part: a mock server is automatically created for every project – look at <a href="http://private-b604b-cheesecakelabs.apiary-mock.com/cakes/">this</a>! See the magic? No code was needed to make it work. Anyone with basic knowledge of HTTP and Blueprint can create a mock API and get feedback from customers.</p>
<p>The example above is as simple as it can get. You can check the Blueprint <a href="https://apiblueprint.org/documentation/tutorial.html">tutorials and documentation</a> to go deeper into its syntax and read the specs <a href="https://github.com/apiaryio/api-blueprint/blob/master/API%20Blueprint%20Specification.md">here</a>. Also, this is an open-source project – any contribution from the community is welcomed.</p>
<p>Congratulations! Now you are armed with the knowledge necessary to design your APIs. But before you start rocking your systems, here are some little tips to keep on your toolbelt:</p>
<h1 id="dos-and-donts">Dos and Don&rsquo;ts</h1>
<p>We have walked a long path together, from understanding what is an API and why its design matters, passing by the HTTP protocol and landing on Blueprint API, giving you the foundation to start creating your own designs. I’ll list some amazing dos and don&rsquo;ts. Note that these aren’t rules, just best practices you generally find on the web.</p>
<h2 id="treat-endpoint-actions-as-crudl-operations">Treat endpoint actions as CRUD(L) operations</h2>
<p>Our /cakes/ endpoint may have Create, Read, Update, Delete and List operations. You can construct these actions with HTTP verbs and the URL, like:</p>
<ul>
<li><strong>List</strong>: <code>GET /cakes/</code></li>
<li><strong>Create</strong>: <code>POST /cakes/</code></li>
<li><strong>Read</strong>: <code>GET /cakes/1/</code></li>
<li><strong>Update</strong>: <code>PATCH /cakes/1/</code></li>
<li><strong>Delete</strong>: <code>DELETE /cakes/1/</code></li>
</ul>
<h2 id="correctly-use-the-http-methods">Correctly use the HTTP methods</h2>
<p>GET is for getting, POST if for posting. Don’t use POST if you just want a list of Cakes.</p>
<h2 id="http-has-methods-use-them">HTTP has methods, use them!</h2>
<p><code>POST /cakes/createCake</code>
You don’t need to specify the action in the URL, we already know that POST creates something.</p>
<p><code>POST /cakes/</code>
Cleaner URL, also telling us that probably the other methods will work as expected, like GET /cakes.</p>
<h2 id="singular-or-plural-plural">Singular or Plural? Plural!</h2>
<p><code>GET /cakes</code> should return a list of Cakes, so <code>GET /cake/1</code> should return the first Cake, right? Unfortunately, no. Even though it makes sense in our language, it will just confuse clients and developers with one more endpoint.</p>
<h2 id="searching-ordering-limiting-use-query-parameters">Searching, ordering, limiting? Use query parameters!</h2>
<p>This feature allows you to specify some filtering on List endpoints, here’s an example:</p>
<p><code>GET /cakes/?name=apple</code>
If implemented, should list all the Cakes with apple in the name.</p>
<p><code>GET /cakes/?name=apple&amp;rating=4</code>
You can also concatenate parameters with &amp;, search for apple and a 4 rating.</p>
<h2 id="return-errors-with-4xx">Return. Errors. With. 4xx.</h2>
<p>If you want to take just one thing from this article, make sure it’s this one: everybody freaking hates a response with HTTP 2xx and a message of error! Use the correct codes:</p>
<ul>
<li><strong>401</strong>: Unauthorized access, where the authorization process wasn&rsquo;t done correctly.</li>
<li><strong>403</strong>: Forbidden access, the client is authorized, but has no access to the resource.</li>
<li><strong>404</strong>: The famous not found, indicating the resource is not available.</li>
</ul>
<h2 id="describe-your-errors-with-clarity">Describe your errors with clarity</h2>
<p>When something fails, inform the client about what happened and how it can recover. Here’s a nice way to do it:</p>
<div class="highlight"><pre tabindex="0" style="color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4;"><code class="language-json" data-lang="json"><span style="display:flex;"><span>{
</span></span><span style="display:flex;"><span>  <span style="color:#f92672">&#34;error&#34;</span>: {
</span></span><span style="display:flex;"><span>    <span style="color:#f92672">&#34;type&#34;</span>: <span style="color:#e6db74">&#34;Authentication Error&#34;</span>,
</span></span><span style="display:flex;"><span>    <span style="color:#f92672">&#34;details&#34;</span>: <span style="color:#e6db74">&#34;Authentication could not be established with the given username and password&#34;</span>,
</span></span><span style="display:flex;"><span>   }
</span></span><span style="display:flex;"><span>}
</span></span></code></pre></div><p>Everyone understands that.</p>
<h2 id="im-a-teapot">I’m a teapot</h2>
<p>Implement the <a href="https://en.wikipedia.org/wiki/Hyper_Text_Coffee_Pot_Control_Protocol">status 418</a> in a HTTP response. You know, just for fun!</p>
<h2 id="resources-are-like-object-classes">Resources are like object classes</h2>
<p>The API endpoints will respond with a resource representation. Think about these resources as object classes, they then to represent things in the real world.</p>
<p>I could write a lot of tips here, but this post would become a book. And there’s already a good one, <a href="https://apisyouwonthate.com/">Build APIs You Won’t Hate</a> by Phil Sturgeon.</p>
<p>Trust me on this, using a Design-first philosophy will give you better nights of sleep. Here are some advantages and characteristics of a good API:</p>
<ul>
<li><strong>Talk to your customers</strong>: understand what they need, not what they want. An API without clients is nothing but a bad API.</li>
<li><strong>Easy to use</strong>: endpoints, resources and output data should follow the same structure as much as possible.</li>
<li><strong>Hard to misuse</strong>: if a bad request is made, return an error and be informative.</li>
<li><strong>Simple is better than complex</strong>: as a Pythonista, this is carved into my heart. Simple things are easy, in every aspect.</li>
<li><strong>Use your API before implementing it</strong>: create a mock server to get a feel of the end result. If you can, talk to your future customers and ask their opinions.</li>
<li><strong>Be resilient</strong>: when a crash happens, informe why and how to handle the situation.</li>
<li><strong>Test everything</strong>. Really. Write tests for every endpoint, method, parameter, input and output data.</li>
<li>At the end of the day, your API is a new little language you’ll have to teach to other people.</li>
</ul>
<h1 id="im-almost-done-just-some-references">I’m almost done, just some references</h1>
<p>If more tips could fit a book, talking about all aspects of API Design would fit a library. I’ll leave you, my patient reader, with some amazing references on further topics:</p>
<ul>
<li><a href="https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol">HTTP on Wikipedia</a> - a good reading about the Hypertext Transfer Protocol.</li>
<li><a href="https://www.youtube.com/watch?v=aAb7hSCtvGw">How To Design A Good API and Why it Matters</a> - a talk by Joshua Bloch Java APIs, but the concepts apply to the web as well. Here are the slides.</li>
<li><a href="http://shop.oreilly.com/product/0636920028468.do">RESTful Web APIs</a> by By Leonard Richardson, Mike Amundsena and Sam Ruby - learn everything about  the web, HTTP, hypermedia and domain-specific designs.</li>
<li><a href="https://spring.io/understanding/HATEOAS">HATEOAS</a> - one of the constraints of RESTful APIs.</li>
<li><a href="http://jsonapi.org/">JSON API</a> - a specification for build APIs in JSON.</li>
</ul>
<h1 id="wrapping-up">Wrapping up</h1>
<p>API Design matters. It tends to stop people from just hacking things together to take a step back and see the bigger picture. Here at Cheesecake Labs we do our best to develop APIs following the best practices. We know that it seems time-consuming, but in the long run it pays off!</p>

</div>


        </main>

        <footer>
            <p style="text-align: center;"><strong>///</strong></p>
            <p>Esse site não rastreia você e é servido pelo<a href="https://codeberg.org/jonatasbaldin/pages"> Codeberg</a>.</p>
            <p><strong>Siga o conteúdo pelo <a href="/rss.xml">RSS</a></strong>.</p>
        </footer>

    </body>
</html>